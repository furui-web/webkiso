<!doctype html>
<html LANG="ja">
    <head>
        <meta charset="UTF-8">
        <title>変数と文字列の出力</title>
    </head>
    <body>
    
<?php
$name = "古井";
$Name = "匠";

echo '私の名前は' .$name .'です。' .'<br />';

echo "私の名前は{$Name}です。<br>";

$book = 'PHP逆云々';
$text = <<<EOL
       ヒアドキュメントで変数に文章を代入します。<br>
       書籍名: $book<br>
EOL;

echo $text;


?>
    </body>
</html>
