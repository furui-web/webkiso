<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
  // put your code here
//変数に整数を代入  
$a = 123;
//変数に文字列を代入
$b = '321';

echo '$a の型: ' . gettype($a) . '<br>';
echo '$b の型: ' . gettype($b) . '<br><br>';

//関数を使い型を変換
$a = strval($a);
$b = intval($b);

echo '$a の型: ' . gettype($a) . '<br>';
echo '$b の型: ' . gettype($b) . '<br><br>';

//型キャストで型を変換
$a = (int) $a;
$b = (string) $b;

echo '$a の型: ' . gettype($a) . '<br>';
echo '$b の型: ' . gettype($b) . '<br><br>';

$c = 'abc';
$c = (int) $c;

echo '$c の型: ' . gettype($c) . '<br>';
echo '$cの中身: ' . $c . '<br><br>';

        
?>
    </body>
</html>
